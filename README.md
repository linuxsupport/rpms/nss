# nss

**THIS PROCESS IS NO LONGER REQUIRED AS OF NOVEMBER 2019**
**REFERENCE: https://its.cern.ch/jira/browse/LOS-376**

This repository contains a script which will rebuild the src rpm of a provided nss version, adding the CERN CA certs to the build.

The script also tests the resulting RPM to ensure that the CERN CA certs have actually been injected successfully.

Here is an example of what should be seen:
```
$ ./rebuildnss.sh 3.44.0-4.el7
Setting up temporary directory at /tmp/tmp.Q0eC5NeXZu ... 
Trying to download from http://vault.centos.org/centos/7.7.1908//os/Source/SPackages/nss-3.44.0-4.el7.src.rpm
Extracting nss-3.44.0-4.el7.src.rpm
Appending CERN specifics to /tmp/tmp.Q0eC5NeXZu/SPECS/nss.spec
Rebuilding SRPM
Wrote: /tmp/tmp.Q0eC5NeXZu/SRPMS/nss-3.44.0-4.el7.cern.src.rpm
Building RPM, this may take awhile ...
Extracting RPM to confirm that the CERN certs are present in /usr/lib64/nss/libnssckbi.so
Build was successful. The following CERN certs are present in libnssckbi.so
Builtin Object Token:CERN Root CA                            C,C,C
Builtin Object Token:CERN Root Certification Authority 2     C,C,C
Builtin Object Token:CERN Grid Certification Authority       c,c,c
Builtin Object Token:CERN Certification Authority            c,c,c
Builtin Object Token:CERN Certification Authority (1)        c,c,c
$
```

The src.rpm file should be transferred to `build@lxsoftadm01:~/packages/incoming/{slc6,cc7}/{cern,updates}` and the standard release process should be followed to promote the RPM into CERN.