#!/bin/bash

QUIET=1
DISABLETEST=1
VER=$1

if [ $# -ne 1 ]; then
  echo "$0 <version to build>"
  exit
fi
if [[ ${VER} == *"el8"* ]]; then
  echo "nss no longer needs to be updated in el8"
  exit
elif [[ ${VER} == *"el7"* ]]; then
  REPOURLS="/os/Source/SPackages /updates/Source/SPackages"
  DIST=7
  TAG=.el${DIST}.cern
else
  REPOURLS="/updates/Source/SPackages"
  DIST=6
  TAG=.el6_10.cern
fi
FILENAME="nss-${VER}.src.rpm"
WORKDIR=`mktemp -d`
mkdir ${WORKDIR}/SOURCES
mkdir ${WORKDIR}/SPECS

echo "Setting up temporary directory at ${WORKDIR} ... "
URL="http://vault.centos.org/centos/${DIST}/${URLSUFFIX}/${FILENAME}"
DOWNLOAD=0
for URLSUFFIX in $REPOURLS; do
  for test in `curl --silent http://vault.centos.org/centos/ | grep "href=\"${DIST}" | awk '{print $7}' | cut -d\> -f2 | cut -d\/ -f1 | sort -r`; do
    URL="http://vault.centos.org/centos/${test}/${URLSUFFIX}/${FILENAME}"
    echo "Trying to download from $URL"
    curl --silent -o ${WORKDIR}/SOURCES/${FILENAME} ${URL}
    TEST=`file ${WORKDIR}/SOURCES/${FILENAME} | grep -q RPM`
    if [ $? -eq 0 ]; then
      DOWNLOAD=1
      break
    fi
  done
done
if [ "${DOWNLOAD}" -eq 0 ]; then
  echo "Unable to download ${FILENAME}"
  exit
fi

cp certdata.cern.txt ${WORKDIR}/SOURCES
cd ${WORKDIR}/SOURCES
echo "Extracting ${FILENAME}"
rpm2cpio nss*src.rpm | cpio -idmv >/dev/null 2>&1
rm -f nss*src.rpm
mv nss.spec ../SPECS/

echo "Appending CERN specifics to ${WORKDIR}/SPECS/nss.spec"
TMPLINENUM=`grep -n ^Source[0-9][0-9]: ${WORKDIR}/SPECS/nss.spec |tail -n1 |cut -d: -f1`
INSERTLINENUM=`echo "${TMPLINENUM} + 1" | bc`
sed -i "${INSERTLINENUM}iSource100:        certdata.cern.txt" ${WORKDIR}/SPECS/nss.spec
TMPLINENUM=`grep -n ^%build ${WORKDIR}/SPECS/nss.spec |tail -n1 |cut -d: -f1`
INSERTLINENUM=`echo "${TMPLINENUM} - 1" | bc`
sed -i "${INSERTLINENUM}i### Add CERN certs to built-ins.\npushd nss/lib/ckfw/builtins/\ncp -v certdata.txt certdata.txt.precern\ncat %{SOURCE100} >> certdata.txt\npopd\n" ${WORKDIR}/SPECS/nss.spec
TMPLINENUM=`grep -n ^%changelog ${WORKDIR}/SPECS/nss.spec |tail -n1 |cut -d: -f1`
INSERTLINENUM=`echo "${TMPLINENUM} + 1" | bc`
VERSION=`sed -n "${INSERTLINENUM}p" ${WORKDIR}/SPECS/nss.spec | cut -d\> -f2 | awk '{print $2}'`
sed -i "${INSERTLINENUM}i* `date "+%a %b %d %Y"` Linux Support <linux.support@cern.ch> - ${VERSION}.cern\n- Add CERN CA\n" ${WORKDIR}/SPECS/nss.spec
echo "Rebuilding SRPM"
rpmbuild -bs --define "_topdir ${WORKDIR}" --define "dist ${TAG}" ${WORKDIR}/SPECS/nss.spec
echo "Building RPM, this may take awhile ..."
if [ "${QUIET}" -eq 1 ]; then
  DISABLETEST=${DISABLETEST} rpmbuild -bb --define "_topdir ${WORKDIR}" --define "dist ${TAG}" ${WORKDIR}/SPECS/nss.spec >/dev/null 2&>1
else
  DISABLETEST=${DISABLETEST} rpmbuild -bb --define "_topdir ${WORKDIR}" --define "dist ${TAG}" ${WORKDIR}/SPECS/nss.spec
fi
echo "Extracting RPM to confirm that the CERN certs are present in /usr/lib64/nss/libnssckbi.so"
cd ${WORKDIR}
rpm2cpio RPMS/x86_64/nss*.rpm | cpio -idmv >/dev/null 2>&1
mkdir cerntest
certutil --empty-password -N -d  cerntest
ln -s ${WORKDIR}/usr/lib64/nss/libnssckbi.so cerntest
NUMCERT=`certutil -L -d sql:${WORKDIR}/cerntest/ -h 'Builtin Object Token' |grep CERN |wc -l`
if [ $NUMCERT -eq 5 ]; then
  echo "Build was successful. The following CERN certs are present in libnssckbi.so"
  certutil -L -d sql:${WORKDIR}/cerntest/ -h 'Builtin Object Token' |grep CERN
else
  echo "Build failed! CERN certs are not present in libnssckbi.so"
  exit 1
fi
